// Find users with "s" in their firstName or "d" on their lastName
db.users.find({$or: 
        [
            {"firstName": {$regex: "s", $options: "$i"}},            
            {"lastName":{$regex:"d", $options:"$i"}}
        ]   
    },
    {
        "firstName":1,
        "lastName":1,
        "_id": 0
    },
);

// Find users who are from HR department and age greater than or equal to 70
db.users.find({$and: [{"department": "HR"}, {"age" : {$gte: 70}}]});

// Users with "e" in their firstName and age less than or equal to 30
db.users.find({$and: 
    [
        {"firstName": {$regex: "e", $options: "$i"}},            
        {"age":{$lte:30}}
    ]   
}
);